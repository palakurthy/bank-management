package 

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Dependents {
	@Id
    @GeneratedValue
	private Long id;

	private Long customerId;
	
	private String name;
	
	private String[] address;

}
